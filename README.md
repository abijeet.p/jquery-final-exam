We are creating a form for a garage that'll aid the garage in servicing vehicles. 

Please create the form below - 

## Fields
-----
1. First name
2. Last name
3. Email
4. Contact number
5. Make
6. Model
7. Year
8. Color
9. Problems

### Notes
---
- The values in the make dropdown can be - Maruti, TATA, Chevrolet, Toyota
- The values in the model depend on the make dropdown.
    - If user selects Maruti, then model dropdown will have - 800 (1995), Alto (2000), WagonR (2002), Esteem (2004) and SX4 (2007)
    - If user selects TATA, then model dropdown will have - Indica (2001), Indigo (2006), Safari (2003), Sumo (2001)
    - If user selects Chevrolet, then model dropdown will have - Beat (2006), Travera (2002), Spark (2007)
    - If user selects Toyota, then model dropdown will have - Camry (2005), Etios (2010), Corolla (2003), Endeavour (2008).
- Next to the car models, you'll notice that we have years (for example - 800 (1995)). This year denotes the manufacturing start date of that model. The year dropdown should be populated on the basis of this. From the manufacturing date of the car to the current year. 
- Color is a dropdown, fill it with possible car colors.
- Problems is multiple textboxes.

---
![Add / Remove](https://gitlab.com/abijeet.p/jquery-final-exam/raw/master/add-remove.png)
---
*An image of the add / remove problems section*

How problems control should work -

- Clicking on add should add another textbox below the last one and the buttons should move down and position themselves next to the new textbox.
- The idea is that the user should be able to enter a list of problems.
- Clicking on minus should remove the last textbox from the list of textboxes.
- After the user has added a car successfully, show the user a message in green saying - "You're car has been added to our garage for servicing.". This message should disappear automatically after 5 seconds.

## Validations
-----
1. All fields are mandatory.
2. Email should have a single @ symbol.
3. Contact number should be 10 digit number.
4. There should be atleast a single problem listed.

## Displaying data
-----
- The list of cars sent for servicing should be shown in a table. 
- The problems with the car should be presented in a list inside a column. 
- The last column in the table should be contain a tick box. The garage mechanic will click on this when a car has been serviced. This should remove the car from the table and from our list.
- Cars should be shown in the ascending order of created on. So the car that was sent for servicing the earliest should be shown first.
